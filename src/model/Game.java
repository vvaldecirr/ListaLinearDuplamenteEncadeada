package model;

import java.util.Scanner;

/**
 * 
 * @author Student: Valdecir (Black Chan)
 * Game made as exercise for Data Structure class at
 * Unigranrio University
 * Teacher: Luciene
 * 
 */
public class Game {
	private DoubleLinkedListCircular list;
	private int direction; // 1 for normal and -1 for reverse
	
	public Game(DoubleLinkedListCircular list) {
		this.setList(list);
		this.setDirection(1);
	}

	/* begin of getters and setters */
	private void setList(DoubleLinkedListCircular list) {
		this.list = list;
	}

	public int getDirection() {
		return this.direction;
	}

	private void setDirection(int direction) {
		this.direction = direction;
	}

	private void setTurnPlayer(Node player) {
		this.list.setSna(player);
	}

	public Node getTurnPlayer() {
		return this.list.getSna();
	}

	public Node getNextPlayer(Node player) {
		if (this.direction == 1) 
			return player.getNextNode();
		else
			return player.getPreviousNode();
	}

	public Node getPreviousPlayer(Node player) {
		if (this.direction == 1) 
			return player.getPreviousNode();
		else
			return player.getNextNode();
	}
	/* end of getters and setters */
	
	// runs game routine for the actual player
	public void playGame() {
		// if haven't enough players for start the game
		if (this.list.isEmpty()) {
			this.showStatus();
			
			System.out.println("\nImpossível iniciar o jogo.");
			
		} else {
			// running game routine
			Scanner input = new Scanner(System.in);
			
			// if just one player added
			if (this.getTurnPlayer().getNextNode() != null) {
				while(this.getNextPlayer(this.getTurnPlayer()) != null) {
					this.showStatus();
					
					System.out.println("Digite o número de uma das seguintes cartas [1, 3, 9, 12]: ");
					int option = input.nextInt();
					
					switch (option) {
					case 1:
						System.out.println("Pulando 1 (um) jogador.\n");
						this.applyCardRole1();
						
						break;
						
					case 3:
						System.out.println("Eliminando o terceiro jogador da vez.\n");
						this.applyCardRole3();
						
						break;
						
					case 9:
						System.out.println("Eliminando o jogador anterior.\n");
						this.applyCardRole9();
						
						break;
						
					case 12:
						System.out.println("Invertendo o sentido do jogo.\n");
						this.applyCardRole12();
						
						break;
						
					default:
						System.out.println("CARTA INVÁLIDA");
					}
				}
			}
			
			input.close();
			
			this.showStatus();
			
			System.out.println("\nFIM DO JOGO. O JOGADOR "+this.getTurnPlayer().getValue()+" É O VENCEDOR!");
		}
	}
	
	// apply card 1 role "JUMP 1 (one) PLAYER"
	private void applyCardRole1() {
		this.setTurnPlayer(this.getNextPlayer(this.getNextPlayer(this.getTurnPlayer())));
	}
	
	// apply card 3 role "DELETE THE THIRD PLAYER COUNT FROM ACTUAL"
	private void applyCardRole3() {
		//remembering the actual player
		Node aux = this.getTurnPlayer();

		// jumping 3 players for delete  
		this.list.removeNode(this.getNextPlayer(this.getNextPlayer(this.getNextPlayer(this.getTurnPlayer()))).getValue());
		
		// verifying if the next player was't removed for pass the turn to him
		if (this.list.findNodeByValue(this.getNextPlayer(aux).getValue()) != null)
			this.setTurnPlayer(this.getNextPlayer(aux));
		else
			this.setTurnPlayer(aux);
	}
	
	// apply card 9 role "REMOVE THE PREVIOUS PLAYER FROM THE GAME"
	private void applyCardRole9() {
		//remembering the actual player
		Node aux = this.getTurnPlayer();

		// removing previous player
		this.list.removeNode(this.getPreviousPlayer(this.getTurnPlayer()).getValue());
		
		// verifying if the next player was't removed for pass the turn to him
		if (this.list.findNodeByValue(this.getNextPlayer(aux).getValue()) != null)
			this.setTurnPlayer(this.getNextPlayer(aux));
		else
			this.setTurnPlayer(aux);
	}
	
	// apply card 12 role "ACTIVATE / DEACTIVATE REVERSE MODE"
	private void applyCardRole12() {
		// changing direction
		if (this.getDirection() == 1)
			this.setDirection(-1);
		else
			this.setDirection(1);
		
		// passing to the turn the next player
		this.setTurnPlayer(this.getNextPlayer(this.getTurnPlayer()));
	}
	
	// returns the actual status of the game
	public String showStatus() {
		String text = "CONFIGURAÇÃO ATUAL DO JOGO:\n";
		String direction = "horário";
		
		if (this.direction != 1)
			direction = "anti-horário";
		
		text += "Sentido: "+direction+"\n";
		text += "Lista de jogadores ativos:\n";
		text += this.list.showList();
		
		if (!this.list.isEmpty())
			text += "\nJogador da vez: [ "+this.getTurnPlayer().getValue()+" ]\n\n";
		
		return text;
	}
}