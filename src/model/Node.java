package model;

/**
 * 
 * @author Valdecir (Black Chan)
 *
 */
public class Node {
	private int value;
	private Node previousNode;
	private Node nextNode;

	public Node(int value) {
		this.setValue(value);
		// this.setNextNode(null); // doesn't needed on Java
	}

	public int getValue() {
		return this.value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public Node getPreviousNode() {
		return this.previousNode;
	}

	public void setPreviousNode(Node previousNode) {
		this.previousNode = previousNode;
	}

	public Node getNextNode() {
		return this.nextNode;
	}

	public void setNextNode(Node nextNode) {
		this.nextNode = nextNode;
	}
}
