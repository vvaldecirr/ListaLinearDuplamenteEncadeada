package model;
/**
 * 
 * @author Valdecir (Black Chan)
 *
 */
public class DoubleLinkedListCircular {
	private Node sna; // some node address

	public DoubleLinkedListCircular() {
		this.setSna(null);
	}

	// begin of getters and setters
	public Node getSna() {
		return this.sna;
	}


	public void setSna(Node sna) {
		this.sna = sna;
	}
	// end of getters and setters

	// answer true if tle list is empty
	public boolean isEmpty(){
		if (this.sna == null)
			return true;
		else
			return false;
	}
	
	// add a Node into the list remembering the first Node inserted
	public void addNode(Node node) {
		// if the list is empty just insert
		if (this.isEmpty())
			this.sna = node;
		
		else if (this.sna.getNextNode() == null) { // if just have one Node on list, point to each other
			node.setNextNode(this.sna);
			node.setPreviousNode(this.sna);
			
			this.sna.setNextNode(node);
			this.sna.setPreviousNode(node);
			
		} else { // if already more than one Node on the list, insert at the end
			// middle pointing
			node.setPreviousNode(this.sna.getPreviousNode());
			node.setNextNode(this.sna);
			
			// previous pointing
			this.sna.getPreviousNode().setNextNode(node);
			
			// next pointing
			this.sna.setPreviousNode(node);
		}
	}
	
	// remove a Node from the list
	public boolean removeNode(int value) {
		// if the list is empty, impossible to remove
		if (this.sna == null)
			return false;
		
		else {
			// if the the Node for to be excluded is the first one, make the last and second Node 
			// point to each other remembering the oldest data inserted
			if (value == this.sna.getValue()) {
				this.sna.getNextNode().setPreviousNode(this.sna.getPreviousNode());
				this.sna.getPreviousNode().setNextNode(this.sna.getNextNode());
				
				this.sna = this.sna.getNextNode();
				
				return true;
				
			} else if (value == this.sna.getPreviousNode().getValue()) { // else if the the Node for to be excluded is the last one
				this.sna.getPreviousNode().getPreviousNode().setNextNode(this.sna);
				this.sna.setPreviousNode(this.sna.getPreviousNode().getPreviousNode());
				
				return true;
				
			} else {
				// search on list for the Node choose to be excluded 
				Node aux = this.findNodeByValue(value);
				
				if (aux != null) {
					// make the previous Node and the next Node point to each other
					aux.getPreviousNode().setNextNode(aux.getNextNode());
					aux.getNextNode().setPreviousNode(aux.getPreviousNode());					
					
					return true;
					
				} else 
					return false;				
			}
		}
	}
	
	// search a Node by value
	public Node findNodeByValue(int value) {
		Node aux = this.sna;
		
		// while didn't found the Node or reach the end of the list, search
		while (aux.getValue() != this.sna.getPreviousNode().getValue()) {
			if (aux.getValue() == value)
				return aux;
			else
				aux = aux.getNextNode();
		}
		
		return null;
	}
	
	// TODO: show the actual status of the list
	public String showList() {
		String text = "";
		
		// if the list isn't empty
		if (!this.isEmpty()) {
			// if just have one element at list
			if (this.sna.getNextNode() == null) 
				text = String.valueOf(this.sna.getValue());
			else {
				Node aux = this.sna;
				
				do {
					text += aux.getValue() + " | ";
					aux = aux.getNextNode();
				} while (aux.getValue() != this.sna.getValue());			
			}
		} else
			text = "lista vazia";
		
		return text;
	}
}