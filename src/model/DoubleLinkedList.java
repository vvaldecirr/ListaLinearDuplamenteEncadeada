package model;

public class DoubleLinkedList {
	private Node fna;

	public DoubleLinkedList() {
		this.setFna(null);
	}

	public Node getFna() {
		return fna;
	}

	public void setFna(Node fna) {
		this.fna = fna;
	}
	
	// isEmpty
	public boolean isEmpty(){
		if (this.fna == null)
			return true;
		else
			return false;
	}
	
	// insertion
	public void addNode(Node node){
		// if the list is empty just add
		if (this.isEmpty()) {
			this.fna = node;
		} else { // if the list is not empty insert at end
			this.fna.setPreviousNode(node);
			node.setNextNode(this.fna);
			this.fna = node;
		}
	}
	
	// TODO: remove
	public Node removeNode(int value){
		// if the list is empty cant remove
		if (!this.isEmpty()) {
			Node found = this.findNodeByValue(value);
			
			// if exist a node with this value than remove 
			if (found != null) {
				// if is the first node
				if (found.getPreviousNode() == null) {
					
					return null;
				} else if (found.getNextNode() == null) { // if is the last
					
					return null;
				} else { // or it's in the middle
					return null;
				}
			}
			else
				return null;
		}else
			return null;		
	}
	
	// search by value
	public Node findNodeByValue(int value){
		Node aux = this.fna;
		
		do {
			if (aux.getValue() == value)
				return aux;
			
			aux = aux.getNextNode();
		} while (aux.getNextNode() != null);
		
		return null;
	}
	
	// TODO: print
	public String showList(){
		String text = "";

		// if the list isn't empty
		if (!this.isEmpty()) {
			// if just have one element at list
			if (this.fna.getNextNode() == null) 
				text = String.valueOf(this.fna.getValue());
			else {
				Node aux = this.fna;
				
				do {
					text += aux.getValue() + " | ";
					aux = aux.getNextNode();
				} while (aux.getNextNode() != null);			
			}
		} else
			text = "lista vazia";
		
		return text;
	}
}