package control;

import model.DoubleLinkedListCircular;
import model.Game;
import model.Node;

/**
 * 
 * @author Valdecir (Black Chan)
 *
 */
public class JogoCartasTDEp2 {

	public static void main(String[] args) {

		// filling the N players
		DoubleLinkedListCircular l1 = new DoubleLinkedListCircular();
		l1.addNode(new Node(1));
		l1.addNode(new Node(2));
		l1.addNode(new Node(3));
//		l1.addNode(new Node(4));
//		l1.addNode(new Node(5));
//		l1.addNode(new Node(6));
//		l1.addNode(new Node(7));
//		l1.addNode(new Node(8));
//		l1.addNode(new Node(9));
//		l1.addNode(new Node(10));

		Game g1 = new Game(l1);
				
		System.out.println(g1.showStatus());
		
		g1.playGame();
		
//		System.out.println(l1.showList());
//		
//		l1.removeNode(10);
//		
//		System.out.println(l1.showList());

	}

}