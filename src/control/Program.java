package control;

import model.DoubleLinkedList;
import model.Node;

public class Program {

	public static void main(String[] args) {

		DoubleLinkedList l = new DoubleLinkedList();
		
		
		Node n1 = new Node(5);
		Node n2 = new Node(2);
		Node n3 = new Node(1);
		Node n4 = new Node(7);
		Node n5 = new Node(9);
		
		System.out.println(l.showList());
		
//		l.addNode(n1);
//		l.addNode(n2);
//		l.addNode(n3);
//		l.addNode(n4);
//		l.addNode(n5);
		
		System.out.println(l.showList());
	}

}
